<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateYritusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yrituses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('yksyks', 3)->nullable();
            $table->integer('ykskaks')->nullable();
            $table->string('ykskolm', 3)->nullable();
            $table->integer('yksneli')->nullable();
            $table->string('yksviis', 3)->nullable();
            $table->string('ykskuus', 3)->nullable();
            $table->string('ykskuusyks', 3)->nullable();
            $table->string('ykskuuskaks', 3)->nullable();
            $table->integer('kaksyks')->nullable();
            $table->integer('kakskaks')->nullable();
            $table->integer('kakskolm')->nullable();
            $table->integer('kaksneli')->nullable();
            $table->integer('kaksviis')->nullable();
            $table->integer('kakskuus')->nullable();
            $table->string('kaksseitse', 3)->nullable();
            $table->string('kakskaheksa', 3)->nullable();
            $table->string('kaksyheksa', 3)->nullable();
            $table->integer('kolmyksyks')->nullable();
            $table->integer('kolmykskaks')->nullable();
            $table->integer('kolmykskolm')->nullable();
            $table->integer('kolmyksneli')->nullable();
            $table->integer('kolmkaksyks')->nullable();
            $table->integer('kolmkakskaks')->nullable();
            $table->integer('kolmkakskolm')->nullable();
            $table->integer('kolmkolmyks')->nullable();
            $table->integer('kolmkolmkaks')->nullable();
            $table->integer('kolmneliyks')->nullable();
            $table->integer('kolmnelikaks')->nullable();
            $table->integer('kolmnelikolm')->nullable();
            $table->integer('kolmnelineli')->nullable();
            $table->integer('kolmviis')->nullable();
            $table->string('neliyks', 3)->nullable();
            $table->string('nelikaks', 3)->nullable();
            $table->string('nelikolm', 3)->nullable();
            $table->integer('viisyks')->nullable();
            $table->integer('viiskaks')->nullable();
            $table->integer('viiskolm')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yrituses');
    }
}
